$(window).scroll(function () {
    if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
        $('#return-to-top').fadeIn(200);  // Fade in the arrow
        $(".navbar-light").css('background-image', 'linear-gradient(110deg, #FFFFFF 0%, #FFFFFF 70%)');
        $('.navbar .navbar-collapse .navbar-nav .nav-item .nav-link').css('color', '#566573');
        $('.header_logo_color').css('background-repeat', 'no-repeat')
    } else {
        $('#return-to-top').fadeOut(200);   // Else fade out the arrow
        $(".navbar-light").css('background-image', 'linear-gradient(110deg, #EF186C 0%, #6a5bf1 70%)');
        $('.navbar .navbar-collapse .navbar-nav .nav-item .nav-link').css('color', '#FFFFFF');
        $('.header_logo_white').css('background-repeat', 'no-repeat');
    }
});

$('#return-to-top').click(function () {      // When arrow is clicked
    $('body,html').animate({
        scrollTop: 0                       // Scroll to top of body
    }, 500);
});

//Page load
function myFunction() {
    var logo = $(".white-logo");
    if ($(this).scrollTop() >= 50) {
        $(".navbar-light").css('background-image', 'linear-gradient(110deg, #FFFFFF 0%, #FFFFFF 70%)');
        logo.removeClass('white-logo').addClass("color-logo").fadeIn("slow");
    } else {
        $(".navbar-light").css('background-image', 'linear-gradient(110deg, #EF186C 0%, #6a5bf1 70%)');
        logo.removeClass('color-logo').addClass("white-logo").fadeIn("slow");
    }

    $(window).scroll(function () {
        var scroll = $(window).scrollTop();

        if (scroll >= 50) {
            if (!logo.hasClass("color-logo")) {
                logo.hide();
                $(".navbar-light").css('background-image', 'linear-gradient(110deg, #FFFFFF 0%, #FFFFFF 70%)');
                logo.removeClass('white-logo').addClass("color-logo").fadeIn("fast");
            }
        } else {
            if (!logo.hasClass("white-logo")) {
                logo.hide();
                $(".navbar-light").css('background-image', 'linear-gradient(110deg, #EF186C 0%, #6a5bf1 70%)');
                logo.removeClass("color-logo").addClass('white-logo').fadeIn("fast");
            }
        }
    });
}
